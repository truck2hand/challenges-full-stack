# frontend-service

## Requirements

- Node 12.16.1

## How to develop

```
npm install
npm run dev
```

## How to build

```
npm run build
```
